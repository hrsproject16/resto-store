import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { getLocaleDayNames } from '@angular/common';
import { LoadingService } from '../services/loading.service';
import * as firebase from 'firebase/app';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed } from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild('doughnutCanvas', { static: true }) doughnutCanvas: ElementRef;

  uid: any;
  produk: any = [];
  produkMakanan: any = [];
  produkMinuman: any = [];
  timestamp: any;

  doughnut: Chart;
  day: any[] = [];
  data: any;
  status:boolean;

  constructor(
    public afAuth: AngularFireAuth,
    public db: AngularFirestore,
    private router: Router,
    private http: HttpClient,
    public loadingService: LoadingService
  ) {
    this.afAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getReport();
      this.getUserData(user.uid);
      this.getProduk(user.uid);
      this.getMakanan(user.uid);
      this.getMinuman(user.uid);
      this.getPesananDataMasuk(user.uid);
    }, error => {
      this.router.navigate(['/login']);
    });
    var d = new Date();
    var hari = new Array(7);
    hari[0] = "Minggu";
    hari[1] = "Senin";
    hari[2] = "Selasa";
    hari[3] = "Rabu";
    hari[4] = "Kamis";
    hari[5] = "Jumat";
    hari[6] = "Sabtu";
    this.day = hari[d.getDay()];
  }

  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();

    // On succcess, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        this.saveToken(token.value);
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.log('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        var audio1 = new Audio('assets/audio/pristine.mp3');
        audio1.play();
        // alert('Push received: ' + JSON.stringify(notification));
        console.log('Push received: ', notification);

        let alertRet = Modals.alert({
          title: notification.title,
          message: notification.body
        });
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        this.router.navigateByUrl('/pesanan-masuk');
      }
    );
  }

  pesananDataMasuk: any = [];
  getPesananDataMasuk(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('warung', '==', uid).where('status', '==', 'masuk').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataMasuk = res;
    })
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 2000);
  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.status = this.userData.status;
    });
  }

  getProduk(uid) {
    this.db.collection('produk', ref => {
      return ref.where('id_warung', '==', uid);
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
    })
  }

  minuman: {};
  getMinuman(uid) {
    this.db.collection('produk', ref => {
      return ref.where('id_warung', '==', uid).where('kategori', '==', 'minuman');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produkMinuman = res;
      this.minuman = [res.length];
    })
  }

  // parseMinuman(res) {
  //   this.minuman = res.reduce.length;
  //   console.log(this.minuman);
  // }

  makanan: {};
  getMakanan(uid) {
    this.db.collection('produk', ref => {
      return ref.where('id_warung', '==', uid).where('kategori', '==', 'makanan');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produkMakanan = res;
      this.makanan = [res.length];
    })
  }

  //status akun

  loadingAction: any = {};
  aktivasi() {
    var dt = {
      status: this.status
    };

    this.db.collection('owner').doc(this.uid).update(dt).then(res => {
      this.loadingAction = {};
    });
    this.loadingService.present({
      message: 'Tunggu yaa..',
      duration: 1000
    });
  }

  saveToken(token) {
    const data = {
      token,
      userId: this.uid,
      datetime: this.timestamp
    };
    this.db.collection('devices').doc(token).set(data).then(res=>{
      
    });
  }

  ionViewDidEnter() {
    this.createdoughnutCanvas();
  }

  //get laporan
  reports:any=[];
  reportsMinuman:any=[];
  reportsMakanan:any=[];
  reportsToday:any=[];
  oneDay=86400000;
  getReport()
  {
    var uid = this.uid;
    this.db.collection('pesanan',ref=>{
      return ref.where('warung', '==', uid).where('status', '==', 'selesai');
    }).valueChanges({idField:'id'}).subscribe(res=>{
      this.parseReport(res);     
    })
  }

  parseReport(data)
  { 
    var last7Day=new Date(new Date().getTime()-(this.oneDay*7));
    var t=new Date();
    var today= new Date(new Date().getTime());
    var to = today.toDateString();
    var dataMakanan = [];
    var dataMinuman = [];
    for(var i=0; i<data.length; i++)
    {
      var sekarang = new Date(new Date(data[i].waktu.toDate()).getTime());
      var s = sekarang.toDateString();

      var waktu = new Date(data[i].waktu.toDate());
      if(s == to) {
        if(data[i].infoOrder.kategori == 'makanan') {
          dataMakanan.push(data[i]);
        } else if(data[i].infoOrder.kategori == 'minuman') {
          dataMinuman.push(data[i]);
        }
      }
      // var waktu = new Date(data[i].waktu.toDate());
      // if(waktu > last7Day) {
      //   this.reports.push(data[i]);
      // }    
    }
    this.reportsMakanan = dataMakanan;
    this.reportsMinuman = dataMinuman;
    this.rataOrderMakanan(dataMakanan);
    this.rataOrderMinuman(dataMinuman);
    this.createdoughnutCanvas();
  }

  ratarataMakanan= 0;
  rataOrderMakanan(dataMakanan) {
    let total = 0;
    for(var i=0; i<dataMakanan.length; i++){
      total = total + dataMakanan.length;
    }

    let ratarataMakanan = 0;
    ratarataMakanan = total/dataMakanan.length;
    this.ratarataMakanan = ratarataMakanan;
  }

  ratarataMinuman= 0;
  rataOrderMinuman(dataMinuman) {
    let total = 0;
    for(var i=0; i<dataMinuman.length; i++){
      total = total + dataMinuman.length;
    }

    let ratarataMinuman = 0;
    ratarataMinuman = total/dataMinuman.length;
    this.ratarataMinuman = ratarataMinuman;
  }

  createdoughnutCanvas() {
    var jumMakanan = this.ratarataMakanan;
    var jumMinuman = this.ratarataMinuman;
    this.doughnut = new Chart(this.doughnutCanvas.nativeElement, {
      type: "doughnut",
      data: {
        labels: ["Makanan", "Minuman"],
        datasets: [
          {
            label: "Data Penjualan Hari Ini",
            data: [jumMakanan, jumMinuman],
            fill: false,
            lineTension: 0.1,
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)"
            ],
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            spanGaps: false,
          }
        ]
      },
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        tooltips: {
          enabled: true,
          mode: 'single',
          callbacks: {
            label: function (tooltipItems, data) {
              return data.datasets[0].data[tooltipItems.index] + ' %';
            }
          }
        }
      }
    });
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
}
