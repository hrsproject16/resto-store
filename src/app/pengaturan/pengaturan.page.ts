import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { ImageCropperComponent } from '../image-cropper/image-cropper.component';
import { from } from 'rxjs';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-pengaturan',
  templateUrl: './pengaturan.page.html',
  styleUrls: ['./pengaturan.page.scss'],
})
export class PengaturanPage implements OnInit {

  uid:any;
  avatar:any;
  data: any;

  constructor(
    public fsAuth:AngularFireAuth, 
    private router:Router, 
    private db:AngularFirestore,
    public storage: AngularFireStorage,
    public alertController: AlertController,
    public loadingService: LoadingService,
    public actionSheetController: ActionSheetController,
    public modalController: ModalController
  ) {  
      this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
      this.getUserData(user.uid);
    });
  }

  ngOnInit() {
      // this.loadingService.present({
      // message: 'Menyiapkan Data',
      // duration: 2500
    // });
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 2000);
  }


  keluar()
  {
    this.alertConfirm();
  }

  kembali() {
    this.router.navigate(['/home']);
  }

   //confirm
   async alertConfirm() {
    const alert = await this.alertController.create({
      header: 'Keluar',
      message: 'Apakah Anda yakin?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.fsAuth.auth.signOut().then(res=>{
              this.router.navigate(['/login']);
            })
            
          }
        }
      ]
    });
    await alert.present();
  }

  async ActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto Sampul',
      buttons: [{
        text: 'Kamera',
        icon: 'camera',
        handler: () => {
          this.takeKamera();
        }
      }, {
        text: 'Buka Galeri Foto',
        icon: 'image',
        handler: () => {
          this.takeGaleri();
          console.log('Share clicked');
        }
      }, {
        text: 'Batal',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  userData:any={};
  getUserData(uid)
  {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res=>{
        this.userData=res;
        this.getImage(this.userData.avatar);       
    })
  }

  loading:boolean;
  updateData()
  {
      this.loading=true;
      this.db.collection('owner').doc(this.uid).update(this.userData).then(res=>{
        swal({   
          title: "Sukses",   
          text: "Data berhasil diperbarui.",   
          icon: "success",
          timer: 2000,   
        });
        this.loading=false;
      })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      subHeader: 'Berhasil',
      message: 'Data berhasil diperbarui',
      buttons: ['OK']
    });

    await alert.present();
    this.router.navigate(['/pengaturan']);
  }

  async takeKamera() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.avatar = image.dataUrl;
    this.cropImage(this.avatar,1);
  }

  async takeGaleri() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    });
    this.avatar = image.dataUrl;
    this.cropImage(this.avatar,1);
  }

  async cropImage(avatar,rasio)
  {    
    const modal = await this.modalController.create({
      component: ImageCropperComponent,
      componentProps: {
        imageData:avatar,
        rasio:rasio
      }
    });
    modal.onDidDismiss().then(res => {
      if(res.data.dismissed == false)
      {
        this.updateAvatar(res.data.imageData);
      }
    });
    return await modal.present();
  }

  loadingAction: any = {};
  loadingUpload() {
    this.loadingService.present({
      message: 'Sedang mengunggah foto..',
      duration: 1000
    });
  }

  data_foto: any;
  updateAvatar(imageData)
  {
    this.loadingUpload();
    var filename = this.uid + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => { this.data_foto = snapshot;    
      this.updateUser(filename);
    });
  }

  updateUser(fn)
  {
    var dt={
      avatar:fn
    };
    this.db.collection('owner').doc(this.uid).update(dt).then(dat=>{
      swal({   
        title: "Sukses",   
        text: "Data berhasil diperbarui.",   
        icon: "success",
        timer: 2000,   
      });
      return;
    })
  }

  images:any={};
  getImage(ref)
  {
      if (ref != null && this.images[ref]==undefined)
      {
        this.images[ref] = '';
        this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
          this.avatar = res;
        });
      }
  }
 



}
