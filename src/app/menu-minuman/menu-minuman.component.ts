import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { TambahMinumanComponent } from '../tambah-minuman/tambah-minuman.component';
import { EditMenuMinumanComponent } from '../edit-menu-minuman/edit-menu-minuman.component';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-menu-minuman',
  templateUrl: './menu-minuman.component.html',
  styleUrls: ['./menu-minuman.component.scss'],
})
export class MenuMinumanComponent implements OnInit {

  id: any;
  searchTerm: string = '';
  produk: any = [];
  kategori: any = [];
  dataDetail: any = [];
  data: any;

  constructor(
    public fsAuth: AngularFireAuth,
    private route: ActivatedRoute,
    public db: AngularFirestore,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private router: Router,
    public storage: AngularFireStorage,
    public modalController: ModalController,
    public loadingService: LoadingService
  ) {
    this.id = this.route.snapshot.paramMap.get("uid");
    console.log(this.id);
    this.getProduk(this.id);
    this.getUserData(this.id);
  }

  ngOnInit() {
    // this.loadingService.present({
    //   message: 'Menyiapkan Data',
    //   duration: 3000
    // });
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.data = {
        'heading': 'Normal text',
        'para1': 'Lorem ipsum dolor sit amet, consectetur',
        'para2': 'adipiscing elit.'
      };
    }, 2000);
  }


  async openViewer() {
    const modal = await this.modalController.create({
      component: ViewerModalComponent,
      componentProps: {
        src: "./assets/img/demo.jpg"
      },
      cssClass: 'ion-img-viewer',
      keyboardClose: true,
      showBackdrop: true
    });

    return await modal.present();
  }

  getProduk(id) {
    this.db.collection('produk', ref => {
      return ref.where('id_warung', '==', id).where('kategori', '==', 'minuman');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
      this.parseData(res);
      console.log(this.produk);
    })
  }

  parseData(data) {
    for (var i = 0; i < data.length; i++) {
      if (data[i].url != undefined) {
        this.getImage(data[i].url);
      }
    }
  };

  avatar: any = {};
  getImage(ref) {
    if (this.avatar[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.avatar[ref] = url;
      });
    }
  }

  kembali() {
    this.router.navigate(['/home']);
  }

  userData: any = {};

  getUserData(id) {
    this.db.collection('owner').doc(id).valueChanges().subscribe(res => {
      console.log(res);
      this.userData = res;
    });
  }

  initializeItems(): void {
    this.produk = this.kategori;
  }

  searchChanged(evt) {

    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.produk = this.produk.filter(produk => {
      if (produk.nama && searchTerm) {
        if (produk.nama.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  hapus(rowID) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('produk').doc(rowID).delete();
    } else {
      return;
    }
  }

  item: any;
  getData(item) {
    var data = item;
    this.db.collection('produk').doc(item.id).valueChanges().subscribe(res => {
      this.dataDetail = res;
      this.item = res;
      console.log(this.dataDetail);
      this.tambahEdit(data);
    })
  }

  async tambah() {
    const modal = await this.modalController.create({
      component: TambahMinumanComponent,
      componentProps: {
        "userData": this.userData,
        "id": this.id
      }
    });
    return await modal.present();
  }
  async tambahEdit(data) {
    const modal = await this.modalController.create({
      component: EditMenuMinumanComponent,
      componentProps: {
        "data": this.dataDetail,
        "item": data
      }
    });
    return await modal.present();
  }

}

