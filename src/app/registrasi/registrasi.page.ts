import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { SyaratKetentuanComponent } from '../syarat-ketentuan/syarat-ketentuan.component';
import { from } from 'rxjs';
import * as firebase from 'firebase';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-registrasi',
  templateUrl: './registrasi.page.html',
  styleUrls: ['./registrasi.page.scss'],
})
export class RegistrasiPage implements OnInit {

  timestamp: any;

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    public toastController:ToastController,
    public modalCtrl:ModalController
  ) { }

  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
  }

  loading:boolean;
  register(email, password, nama, nama_owner, alamat, bio) {
    this.loading=true;
    this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(user=>{       
       this.afAuth.auth.currentUser.updateProfile({
        displayName:nama
      }).then(res=>{
        this.updateProfile(email, nama, nama_owner, alamat, user.user.uid, bio);
        this.sendEmailVerification(email);
      })
    }).catch(error=>{
      this.toastAlert(error.message);
      this.loading=false;
    })
  }

  sendEmailVerification(email) {
    this.afAuth.authState.subscribe(user => {
        user.sendEmailVerification()
        .then(() => {
          swal({   
            title: "Sukses",   
            text: "Email verifikasi terkirim.",   
            icon: "success",
            timer: 2000,   
          });
          this.router.navigate(['verifikasi-email']);
        })
    });
  }

  async detailSyarat() {
    const modal = await this.modalCtrl.create({
      component: SyaratKetentuanComponent,
    });

    return await modal.present();
  }

  async toastAlert(msg){
    const toast=await this.toastController.create({     
      message:msg,
      duration:3000,
      position:'bottom'
    });
    toast.present();
  }

  updateProfile(email, nama, nama_owner, alamat, uid, bio)
  {
    var dt={
      role:'store',
      nama:nama,
      nama_owner:nama_owner,
      alamat:alamat,
      email:email,
      bio:bio,
      created_at: this.timestamp,
      penjualan: 0,
      rating: 0,
      pendapatan: 0,
      verifikasi: false
    };
    this.db.collection('owner').doc(uid).set(dt).then(res=>{
      swal({   
        title: "Sukses",   
        text: "Pendaftaran Berhasil.",   
        icon: "success",
        timer: 2000,   
      });
      this.loading=false;
    })    
  }

  public type = 'password';
  public showPass = false;
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
