import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss'],
})
export class ImageCropperComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  rasio:any;

  constructor(
    public navParams:NavParams,
    private modalCtrl:ModalController
  ) { 
    this.imageChangedEvent=this.navParams.get('imageData');
    this.rasio=this.navParams.get('rasio');
   }

  ngOnInit() {}

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
}
imageLoaded() {
    // show cropper
}
cropperReady() {
    // cropper ready
}
loadImageFailed() {
    // show message
}

add()
{
  this.modalCtrl.dismiss({
    'dismissed': false,
    'imageData':this.croppedImage
  });
}

dismiss() {   
  this.modalCtrl.dismiss({
    'dismissed': true,
  });
}

}
