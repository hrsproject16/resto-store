import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PesananMasukComponent } from './pesanan-masuk/pesanan-masuk.component';
import { MenuMakananComponent } from './menu-makanan/menu-makanan.component';
import { MenuMinumanComponent } from './menu-minuman/menu-minuman.component';
import { EditMenuMinumanComponent } from './edit-menu-minuman/edit-menu-minuman.component';
import { EditMenuMakananComponent } from './edit-menu-makanan/edit-menu-makanan.component';
import { StatusPesananComponent } from './status-pesanan/status-pesanan.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'registrasi',
    loadChildren: () => import('./registrasi/registrasi.module').then(m => m.RegistrasiPageModule)
  },
  {
    path: 'lupa-sandi',
    loadChildren: () => import('./lupa-sandi/lupa-sandi.module').then(m => m.LupaSandiPageModule)
  },
  {
    path: 'status-pesanan/:doc',
    component: StatusPesananComponent
  },
  {
    path: 'pesanan-masuk',
    component: PesananMasukComponent
  },
  {
    path: 'menu-makanan/:uid',
    component: MenuMakananComponent
  },
  {
    path: 'menu-minuman/:uid',
    component: MenuMinumanComponent
  },
  {
    path: 'edit-menu-makanan',
    component: EditMenuMakananComponent
  },
  {
    path: 'edit-menu-minuman',
    component: EditMenuMinumanComponent
  },
  {
    path: 'pengaturan',
    loadChildren: () => import('./pengaturan/pengaturan.module').then(m => m.PengaturanPageModule)
  },
  {
    path: 'tentang',
    loadChildren: () => import('./tentang/tentang.module').then(m => m.TentangPageModule)
  },
  {
    path: 'verifikasi-email',
    loadChildren: () => import('./verifikasi-email/verifikasi-email.module').then(m => m.VerifikasiEmailPageModule)
  },  {
    path: 'rekap',
    loadChildren: () => import('./rekap/rekap.module').then( m => m.RekapPageModule)
  }






];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
