import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tentang',
  templateUrl: './tentang.page.html',
  styleUrls: ['./tentang.page.scss'],
})
export class TentangPage implements OnInit {

  constructor(
    private router:Router
    ) { }

  ngOnInit() {
  }

  kembali() {
    this.router.navigate(['/home']);
  }

}

