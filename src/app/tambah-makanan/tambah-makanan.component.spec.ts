import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahMakananComponent } from './tambah-makanan.component';

describe('TambahMakananComponent', () => {
  let component: TambahMakananComponent;
  let fixture: ComponentFixture<TambahMakananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahMakananComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahMakananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
