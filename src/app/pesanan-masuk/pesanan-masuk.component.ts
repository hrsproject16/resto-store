import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { DetailPesananComponent } from './detail-pesanan/detail-pesanan.component';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-pesanan-masuk',
  templateUrl: './pesanan-masuk.component.html',
  styleUrls: ['./pesanan-masuk.component.scss'],
})
export class PesananMasukComponent implements OnInit {
  uid: any;
  dataReturned: any;
  riwayat:any;

  constructor(
    public fsAuth: AngularFireAuth,
    private route: ActivatedRoute,
    public db: AngularFirestore,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public storage: AngularFireStorage,
    private router: Router,
    public modalController: ModalController,
    public loadingService: LoadingService
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUser(user.uid);
      this.getPesananDataMasuk(user.uid);
      this.getPesananDataDiantar(user.uid);
      this.getPesananDataSelesai(user.uid);
    });

  }

  ngOnInit() {
    this.riwayat = 'masuk';
    this.loadingService.present({
      message: 'Menyiapkan Data',
      duration: 2000
    });
  }

  pesananDataMasuk: any = [];
  getPesananDataMasuk(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('warung', '==', uid).where('status', '==', 'masuk').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataMasuk = res;
      this.parseData(res);
    })
  }

  pesananDataDiantar: any = [];
  getPesananDataDiantar(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('warung', '==', uid).where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataDiantar = res;
      this.parseData(res);
    })
  }

  pesananDataSelesai: any = [];
  getPesananDataSelesai(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('warung', '==', uid).where('status', '==', 'selesai').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataSelesai = res;
      this.parseData(res);
    })
  }

  parseData(data) {
    for (var i = 0; i < data.length; i++) {
      this.getImage(data[i].infoOrder.gambar_produk);
      this.getUserData(data[i].uid);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData[uid] = res;
    })
  }

  data: any = {};
  getUser(uid) {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res => {
      this.data = res;
    })
  }

  async detailPesanan(data) {
    const modal = await this.modalCtrl.create({
      component: DetailPesananComponent,
      componentProps: {
        "orderData": data,
        "mode": 'terima',
        "pemesan": this.userData[data.uid]
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal kirim data :'+ dataReturned);
      }
    });

    return await modal.present();
  }

  diantar(data) {
    if (data.id != null) {
      this.pesananDiantar(data.id);
    }
  }

  pesananDiantar(doc) {
    this.router.navigate(['/status-pesanan/' + doc]);
  }

  kembali() {
    this.router.navigate(['/home']);
  }
  keluar() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}

