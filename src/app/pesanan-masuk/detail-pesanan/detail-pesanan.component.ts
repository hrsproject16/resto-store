import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-detail-pesanan',
  templateUrl: './detail-pesanan.component.html',
  styleUrls: ['./detail-pesanan.component.scss'],
})
export class DetailPesananComponent implements OnInit {
  uid: any;
  orderData: any = {};
  infoOrder: any = {};
  data: any = [];
  timesstamp: any;
  constructor(public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private navParams: NavParams,
    private router: Router,
    public alertController: AlertController,
    public storage: AngularFireStorage,
    private modalCtrl: ModalController,
    private http: HttpClient) {
    {
      this.fsAuth.auth.onAuthStateChanged(user => {
        this.uid = user.uid;
      });
    }
  }
  mode: any;
  pemesan: any = {};
  ngOnInit() {
    this.orderData = this.navParams.get('orderData');
    this.mode = this.navParams.get('mode');
    this.pemesan = this.navParams.get('pemesan');
    this.getPesananData(this.orderData);
    this.getImage(this.pemesan.avatar);
  }

  
  pesananData: any = [];
  getPesananData(data) {
    var order = data.orderData;
    var obj = Object.keys(order);
    for (var i = 0; i < obj.length; i++) {
      var dt = order[obj[i]];
      dt.id = obj[i];
      this.getImage(order[obj[i]].url);
      this.pesananData.push(dt);
    }
  }

  keluar() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  loading: boolean;
  terima() {
    var r = confirm("Terima Pesanan ?");
    if (r == true) {
      this.loading = true;
      var data = {
        warung: this.uid,
        status: "diproses"
      }
      this.db.collection('pesanan').doc(this.orderData.id).update(data).then(res => {
        this.loading = false;
        this.kirimNotifikasi();
        swal({   
          title: "Sukses",   
          text: "Pesanan diterima, silahkan segera diproses.",   
          icon: "success",
          timer: 2000,   
        });
        this.selesai();
        this.router.navigate(['/pesanan-masuk']);
      })
    } else {
      return;
    }
  }

  url_notif = 'https://fcm.googleapis.com/fcm/send';

  kirimNotifikasi() {
    this.db.collection('devices', ref => {
      return ref.where('role', '==', 'runner')
    }).valueChanges().subscribe(res => {
      this.parseDevices(res);
    })
  }

  parseDevices(res) {
    var dataDevices = res;
    var obj = Object.keys(dataDevices);
    for (var i = 0; i < obj.length; i++) {
      let data = {
        "notification": {
          "title": "PesanAja Runner",
          "body": "Ada pesanan baru masuk, cek sekarang!",
          "sound": "default",
          "click_action": "FCM_PLUGIN_ACTIVITY",
          "icon": "fcm_push_icon"
        },
        "to": dataDevices[obj[i]].token,
        "priority": "high",
        "restricted_package_name": ""
      };

      let headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `key=AAAApnCT334:APA91bEE2Ft48A28KZDuB5C2FF0UqpW9Es20IXIr4kjaeMfiTEibOqB1MBsVjBP_aXCZT9fLgEvuT8nePcBrWkcPTm31ItdllvRhdYqiXkTNzZ6e8nsKwzPP69pBo7FDrrdApepJshwJ`
      });

      this.http.post(this.url_notif, data, { headers }).subscribe(res => {
      });
    }
  }

  async tolak() {
    let alert = await this.alertController.create({
      header: 'Alasan',
      inputs: [
        {
          name: 'alasan',
          placeholder: 'Alasan'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Kirim',
          handler: data => {
            this.confTolak(data);
          }
        }
      ]
    });

    await alert.present();
  }

  confTolak(data) {
    var dt = {
      warung: this.uid,
      status: "ditolak",
      alasan: data.alasan
    }
    this.db.collection('pesanan').doc(this.orderData.id).update(dt).then(res => {
      this.loading = false;
      swal({   
        title: "Sukses",   
        text: "Pesanan ditolak.",   
        icon: "success",
        timer: 2000,   
      });
      this.selesai();
      this.router.navigate(['/home']);
    })
  }

  async selesai() {
    const onClosedData: string = "Selesai";
    await this.modalCtrl.dismiss(onClosedData);
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }


}


