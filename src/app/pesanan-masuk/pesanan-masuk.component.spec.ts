import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PesananMasukComponent } from './pesanan-masuk.component';

describe('PesananMasukComponent', () => {
  let component: PesananMasukComponent;
  let fixture: ComponentFixture<PesananMasukComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesananMasukComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PesananMasukComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
