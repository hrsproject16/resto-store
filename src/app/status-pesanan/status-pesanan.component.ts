import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-status-pesanan',
  templateUrl: './status-pesanan.component.html',
  styleUrls: ['./status-pesanan.component.scss'],
})
export class StatusPesananComponent implements OnInit {

  uid: any;
  timestamp: any;
  constructor(public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    public route: ActivatedRoute,
    public storage: AngularFireStorage,
    public router: Router,
    public alertController: AlertController,
    public loadingService: LoadingService) {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getWarungData(user.uid);
    });
  }
  doc: any = [];
  id_produk: any;
  items: any = [];


  ngOnInit() {
    this.loadingService.present({
      message: 'Menyiapkan Data',
      duration: 2000
    });
    this.doc = this.route.snapshot.paramMap.get('doc');
    this.getPesananData();
  }

  dataOr: any = [];
  infoOrder: any = {};
  avatar: any;
  status: any;
  runner: any;

  kembali() {
    this.router.navigate(['/pesanan-masuk']);
  }

  getPesananData() {
    this.db.collection('pesanan').doc(this.doc).valueChanges().subscribe(data => {
      //this.dataOr=[data];   
      this.parsingInfoOrder(data);
    })
  }

  parsingInfoOrder(data) {
    this.infoOrder = data.infoOrder;
    var obj = Object.keys(data.orderData);
    for (var i = 0; i < obj.length; i++) {
  		/* var dt = {
  			infoOrder:data[obj[i]].infoOrder,
  			orderData:data[obj[i]].orderData
  		};
  		this.items.push(dt); */
      //console.log(dt);
      var dt = data.orderData[obj[i]];
      dt.id = obj[i];
      this.getImage(data.orderData[obj[i]].url);
      this.id_produk = data.orderData[obj[i]].id;
      this.getDataProduk(this.id_produk);
      // this.avatar = data.orderData[obj[i]].avatar;
      // this.getImage(this.avatar); 
      this.dataOr.push(dt);
    }
    console.log(this.dataOr);
    this.status = data.status;
    this.runner = data.runner;
    this.getDataPemesan(data.uid);
    this.getRunner(data.runner);
    this.cekData(this.status);
  }

  judul: any;
  isCreate: boolean;

  cekData(status) {
    if (status == 'diproses') {
      this.judul = 'Pesanan Sedang Diproses';
      this.isCreate = true;
    } else {
      this.judul = 'Pesanan Selesai';
      this.isCreate = false;
    }
  }

  DataProduk: any = {};
  getDataProduk(id_produk) {
    this.db.collection('produk').doc(id_produk).valueChanges().subscribe(res => {
      this.DataProduk = res;
      // this.getImage(this.userData.avatar);
    })
  }

  dataPemesan: any = {};
  getDataPemesan(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.dataPemesan = res;
      this.getImage(this.dataPemesan.avatar);
    })
  }

  warungData: any = {};
  getWarungData(uid) {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res => {
      this.warungData[uid] = res;
    })
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  runnerData: any = {};
  getRunner(uid) {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res => {
      this.runnerData = res;
      if(this.runnerData != undefined) {
        this.getImage(this.runnerData.avatar);
      }
      // this.presentAlert();
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      subHeader: 'Berhasil',
      message: 'Runner Ditemukan.',
      buttons: ['OK']
    });

    await alert.present();
    this.router.navigate(['/pesanan-masuk']);
  }

}

