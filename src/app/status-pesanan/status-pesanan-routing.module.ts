import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatusPesananComponent } from './status-pesanan.component';

const routes: Routes = [
  {
    path: '',
    component: StatusPesananComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatusPesananPageRoutingModule { }
