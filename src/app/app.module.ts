import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//components
import { environment } from '../environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TambahMakananComponent } from './tambah-makanan/tambah-makanan.component';
import { TambahMinumanComponent } from './tambah-minuman/tambah-minuman.component';
import { PesananMasukComponent } from './pesanan-masuk/pesanan-masuk.component';
import { MenuMakananComponent } from './menu-makanan/menu-makanan.component';
import { MenuMinumanComponent } from './menu-minuman/menu-minuman.component';
import { EditMenuMakananComponent } from './edit-menu-makanan/edit-menu-makanan.component';
import { EditMenuMinumanComponent } from './edit-menu-minuman/edit-menu-minuman.component';
import { SyaratKetentuanComponent } from './syarat-ketentuan/syarat-ketentuan.component';
import { DetailPesananComponent } from './pesanan-masuk/detail-pesanan/detail-pesanan.component';
import { StatusPesananComponent } from './status-pesanan/status-pesanan.component';
import { ImageCropperComponent } from './image-cropper/image-cropper.component';
import { ChartsModule } from 'ng2-charts';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [AppComponent,
    TambahMakananComponent,
    TambahMinumanComponent,
    PesananMasukComponent,
    MenuMakananComponent,
    MenuMinumanComponent,
    EditMenuMakananComponent,
    EditMenuMinumanComponent,
    SyaratKetentuanComponent,
    DetailPesananComponent,
    StatusPesananComponent,
    ImageCropperComponent],
  entryComponents: [
    TambahMakananComponent,
    TambahMinumanComponent,
    EditMenuMakananComponent,
    EditMenuMinumanComponent,
    SyaratKetentuanComponent,
    DetailPesananComponent,
    ImageCropperComponent,
    StatusPesananComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    AngularFirestoreModule,
    ReactiveFormsModule,
    FormsModule,
    ImageCropperModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SocialSharing,
    File
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
