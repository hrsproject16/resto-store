import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-lupa-sandi',
  templateUrl: './lupa-sandi.page.html',
  styleUrls: ['./lupa-sandi.page.scss'],
})
export class LupaSandiPage implements OnInit {
  email : any;

  constructor(
    private afAuth: AngularFireAuth,
    public alertController: AlertController,
    private navCtrl: NavController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
  }
  
	reset() {
		this.afAuth.auth.sendPasswordResetEmail(this.email).then(() => {
      swal({   
        title: "Sukses",   
        text: "Silakan Cek email anda.",   
        icon: "success",
        timer: 2000,   
      });
			this.navCtrl.pop()
		}).catch(error => {
      console.log(error)
      swal({   
        title: "Gagal",   
        text: error.message,   
        icon: "warning",
        timer: 2000,   
      });
		})
	}

 async showAlert(title, message) {
		let alert = this.alertCtrl.create({
			header: title,
			message: message,
			buttons: ['OK']
		})
	  await (await alert).present();
	}
  
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Perhatian!',
      message: 'Note :  <strong>Pastikan email yang anda masukan benar</strong>!!!',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.reset();
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }



}

