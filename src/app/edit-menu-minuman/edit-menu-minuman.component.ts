import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { AlertController } from '@ionic/angular';
import { ImageCropperComponent } from '../image-cropper/image-cropper.component';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-edit-menu-minuman',
  templateUrl: './edit-menu-minuman.component.html',
  styleUrls: ['./edit-menu-minuman.component.scss'],
})
export class EditMenuMinumanComponent implements OnInit {
  data: any = {};
  userData: any = {};
  item: any;
  dataDetail: any = {};
  url: any;
  constructor(
    public modalCtrl: ModalController,
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private navParams: NavParams,
    public loadingService: LoadingService,
    public storage: AngularFireStorage,
    public alertController: AlertController,
    public router: Router
  ) { }

  ngOnInit() {
    this.dataDetail = this.navParams.get('data');
    this.item = this.navParams.get('item');
  }
  loading: boolean;
  updateData() {
    this.loading = true;
    this.db.collection('produk').doc(this.item.id).update(this.dataDetail).then(res => {
      this.loading = false;
      swal({   
        title: "Sukses",   
        text: "Data berhasil diperbarui.",   
        icon: "success",
        timer: 2000,   
      });
    })
  }
  
  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    }).then(() => {
      window.location.reload();
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      subHeader: 'Berhasil',
      message: 'Data berhasil diperbarui',
      buttons: ['OK']
    });

    await alert.present();
    this.close();
  }

  async takePicture() {
    const image = await Plugins.Camera.getPhoto({
      quality: 70,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.url = image.dataUrl;
    this.cropImage(this.url,1);
  }

  async takeGalery() {
    const image = await Plugins.Camera.getPhoto({
      quality: 70,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    });
    this.url = image.dataUrl;
    this.cropImage(this.url,1);
  }

  id_produk: any;
  async cropImage(url,rasio)
  {    
    const modal = await this.modalCtrl.create({
      component: ImageCropperComponent,
      componentProps: {
        imageData:url,
        rasio:rasio
      }
    });
    modal.onDidDismiss().then(res => {
      if(res.data.dismissed == false)
      {
        var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
        this.id_produk = doc;
        this.updateFotoProduk(res.data.imageData);
      }
    });
    return await modal.present();
  }

  loadingAction: any = {};
  loadingUpload() {
    this.loadingService.present({
      message: 'Sedang mengunggah foto..',
      duration: 3000
    });
  }

  avatar: any;
  updateFotoProduk(imageData) {
    var filename = 'produk/minuman/' + this.item.id_warung + '/' + this.id_produk + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => {
      this.loadingUpload();
      this.avatar = filename;
    });
  }

  updateProduk(fn) {
    var dt = {
      avatar: fn
    };
    this.db.collection('produk').doc(this.id_produk).update(dt).then(dat => {
      return;
    })
  }

  ambilUlangFoto() {
    this.hapusFotoProduk(this.url);
  }

  hapusFotoProduk(imageData) {
    var filename = 'produk/minuman/' + this.item.id_warung + '/' + this.id_produk + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.delete().then(snapshot => {
      this.avatar = null;
    });
  }

}
