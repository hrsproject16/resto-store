import { Component, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Chart } from 'chart.js';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import * as FileSaver from 'file-saver';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';
import {DatePipe} from '@angular/common';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-rekap',
  templateUrl: './rekap.page.html',
  styleUrls: ['./rekap.page.scss'],
})
export class RekapPage {

  @ViewChild('barChartBulanan', {static: true}) barChartBulanan;
  @ViewChild('barChartKategori', {static: true}) barChartKategori;
  bars: any;
  colorArray: any;
    
  //atribut
  uid:any;
  dataPesanan:any=[];
  tahunTransaksi:any=[];
  totalTransaksi:any={};
  selectedTahunTransaksi:any;
  colors:any=['#00AFB9', '#0081A7', '#FDFCDC', '#F07167', '#FED9B7'];

  //atribut chart
  trxByKategori:any={};
  trxBulanan:any={};

  constructor(
    public afAuth: AngularFireAuth,
    private router:Router,
    public db: AngularFirestore,
    private plt: Platform,
    private file: File,
    private socialSharing: SocialSharing,
    public datepipe: DatePipe
  ) {
    this.afAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getDataTransaksi(user.uid);
    }, error => {
      this.router.navigate(['/login']);
    });
  }

  //mengambil data semua transaksi
  getDataTransaksi(uid)
  {
    this.db.collection('pesanan', ref => {
      return ref.where('warung', '==', uid).where('status', '==', 'selesai')
    }).valueChanges({idField: 'id'}).subscribe(res=>{
      this.dataPesanan=res;     
      this.parseData(res);
      this.hitungTotalTransaksi();
    })
  }

  parseData(res) {
    for(var i=0; i<res.length; i++) {
      this.getUser(res[i].uid);
      this.getOwner(res[i].warung);
      this.getRunner(res[i].runner);
    }
  }

  userData: any = {};
  getUser(uid) {
    if (this.userData[uid] === undefined) {
      this.db.collection('members').doc(uid).get().subscribe(res => {
        this.userData[res.id] = res.data();
      });
    }
  }

  ownerData: any = {};
  getOwner(uid) {
    if (this.ownerData[uid] === undefined) {
      this.db.collection('owner').doc(uid).get().subscribe(res => {
        this.ownerData[res.id] = res.data();
      });
    }
  }

  runnerData: any = {};
  getRunner(uid) {
    if (this.runnerData[uid] === undefined) {
      this.db.collection('runner').doc(uid).get().subscribe(res => {
        this.runnerData[res.id] = res.data();
      });
    }
  }

  //hitung total transaksi
  hitungTotalTransaksi()
  {
    var semuaTransaksi=[];
    
    for(var i=0; i<this.dataPesanan.length; i++)
    {
      var data=this.dataPesanan[i];

      var date=new Date(data.waktu.toDate());
      var bulan=date.getMonth()+1;
      var tahun=date.getFullYear();
      
      var trx=Number(this.dataPesanan[i].totalHarga);
      //HITUNG semua transaksi berdasarkan tahun
      if(this.totalTransaksi[tahun] == undefined)
      {
        this.totalTransaksi[tahun]=[trx];
      }else{
        this.totalTransaksi[tahun].push(trx);
      }
      //semuaTransaksi.push(trx);
      //kumpulkan transaksi berdasarkan kategori

      if(this.trxByKategori[tahun] == undefined)
      {
        this.trxByKategori[tahun]={};
        this.trxByKategori[tahun][data.infoOrder.kategori]=[trx];
      }else{
        if(this.trxByKategori[tahun][data.infoOrder.kategori] == undefined)
        {
          this.trxByKategori[tahun][data.infoOrder.kategori]=[trx];
        }else{
          this.trxByKategori[tahun][data.infoOrder.kategori].push(trx);
        }
      }

      //masukkan daftar bulan
      if(this.tahunTransaksi.indexOf(tahun) == -1)
      this.tahunTransaksi.push(tahun);
       
      //buat data trx bulanan
      if(this.trxBulanan[tahun] == undefined)
      {
        this.trxBulanan[tahun]={};
        this.trxBulanan[tahun][bulan]=[trx];
      }else{
        if(this.trxBulanan[tahun][bulan] == undefined)
        {
          this.trxBulanan[tahun][bulan]=[trx];
        }else{
          this.trxBulanan[tahun][bulan].push(trx);
        }        
      }
    }
    this.selectedTahunTransaksi=this.tahunTransaksi[0];
    this.generateTrxBulanan();
    //this.totalTransaksi=semuaTransaksi.reduce(this.sumArray);
    
   
    this.generateTotalTransaksi();
  }

  //generate chart
  generateCharts()
  {
    this.generateTotalTransaksi();
    this.generateTrxBulanan();
  }

  //generate total transaksi 
  generateTotalTransaksi()
  {
    var keys=Object.keys(this.totalTransaksi);
    for(var i=0; i<keys.length; i++)
    {
      if(this.totalTransaksi[keys[i]] != undefined && this.totalTransaksi[keys[i]].length > 0)
      this.totalTransaksi[keys[i]] = this.totalTransaksi[keys[i]].reduce(this.sumArray);
    }
  }

  //generate data transaksi bulanan
  generateTrxBulanan()
  {
    console.log('changed');
    var data=[];
    var label=['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nop','Des'];
    var tahun=this.selectedTahunTransaksi;
    for(var i=0; i<12; i++)
    {      
      if(this.trxBulanan[tahun][i+1] != undefined)
      {
        data[i] = this.trxBulanan[tahun][i+1].reduce(this.sumArray);
      }else{
        data[i]=0;
      }
    }
    this.createBarChartBulanan(label, data);
  }

  //menjumlahkan bilangan dalam array
  sumArray(total, num) {
    return total + num;
  }

  createBarChartBulanan(labelsPeriode, dataPeriode) {
    this.bars = new Chart(this.barChartBulanan.nativeElement, {
      type: 'bar',
      data: {
        labels: labelsPeriode,
        datasets: [{
          data: dataPeriode,
          backgroundColor: this.colors[0], // array should have same number of elements as number of dataset
          borderColor: '#00000',// array should have same number of elements as number of dataset
          borderWidth: 1  
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
            display: false
        },
      }
    });
  }

  kembali() {
    this.router.navigate(['/home']);
  }

  //export excel
  downloadDataTransaksi()
  {
      //console.log(this.dataTransaksi);
      var data=[];
      for(var i=0; i<this.dataPesanan.length; i++)
      {
        var d=this.dataPesanan[i];

        //orderData

        var keys=Object.keys(d.orderData);
        var dataOrder = [];
        for(var j=0; j<keys.length; j++) {
          dataOrder.push(keys[j]);
        }

        //transaksi
        var trx=Number(this.dataPesanan[i].nilaiTransaksi);
        //get images
        var image;
        if(d.avatar != undefined && d.avatar.length > 0)
        {
          image = '=HYPERLINK("'+d.avatar[0].imageUrl+'";"UNDUH BUKTI")';
        }else{
          image = 'Tidak Tersedia';
        }

        var tgl = new Date(d.waktu.toDate());
        var dt={
          kodeTransaksi:d.id,
          waktu:this.datepipe.transform(tgl, 'dd MMMM yyyy'),
          orderData: d.infoOrder.nama,
          totalHarga : d.totalHarga,
          nama_pembeli : this.userData[d.uid].name == undefined ? 'Tanpa Nama':this.userData[d.uid].name,
          nama_warung : this.ownerData[d.warung].nama == undefined ? 'Tanpa Nama':this.ownerData[d.warung].nama,
          nama_runner : this.runnerData[d.runner] == undefined ? 'Tanpa Nama':this.runnerData[d.runner].name,
          status: d.status
        };
        data.push(dt);
      }      
      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
      const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, 'data-penjualan-'+this.selectedTahunTransaksi+'-');
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });

    if(this.plt.is('capacitor')) {
      this.file.writeFile(this.file.dataDirectory, fileName +'.xlsx', buffer, {replace: true}).then(res => {
        this.socialSharing.share(null, null, res.nativeURL, null);
      })
    } else {
      var blob = new Blob([buffer]);
      var a = window.document.createElement('a');
      a.href =  window.URL.createObjectURL(blob);
      a.download = fileName +'.xlsx';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }

}
