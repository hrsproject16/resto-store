import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { ImageCropperComponent } from '../image-cropper/image-cropper.component';
import { AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { from } from 'rxjs';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-tambah-minuman',
  templateUrl: './tambah-minuman.component.html',
  styleUrls: ['./tambah-minuman.component.scss'],
})
export class TambahMinumanComponent implements OnInit {

  data:any={};
  userData:any={};
  produk:any;
  id:any;
  url:any;
  timestamp: any;

  constructor(
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private router:Router,
    private navParams: NavParams,
    public storage: AngularFireStorage,
    public loadingService: LoadingService,
    public alertController: AlertController,
    public modalCtrl:ModalController
  ) {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
  }

  ngOnInit() {
    this.userData = this.navParams.get('userData');
    this.id = this.navParams.get('id');
    this.getProduk(this.id);
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  getProduk(id) {
    this.db.collection('produk', ref =>{
      return ref.where('id_warung', '==', id).where('kategori', '==', 'minuman');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.produk = res;
    })
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      subHeader: 'Berhasil',
      message: 'Data berhasil ditambahkan',
      buttons: ['OK']
    });

    await alert.present();
    this.close();
  }

  async takePicture() {
    const image = await Plugins.Camera.getPhoto({
      quality: 70,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.url = image.dataUrl;
    this.cropImage(this.url,1);
  }

  async takeGalery() {
    const image = await Plugins.Camera.getPhoto({
      quality: 70,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    });
    this.url = image.dataUrl;
    this.cropImage(this.url,1);
  }

  id_produk: any;
  async cropImage(url,rasio)
  {    
    const modal = await this.modalCtrl.create({
      component: ImageCropperComponent,
      componentProps: {
        imageData:url,
        rasio:rasio
      }
    });
    modal.onDidDismiss().then(res => {
      if(res.data.dismissed == false)
      {
        var doc = new Date().getTime() + '' + Math.floor(Math.random() * 10000);
        this.id_produk = doc;
        this.updateFotoProduk(res.data.imageData);
      }
    });
    return await modal.present();
  }

  loadingAction: any = {};
  loadingUpload() {
    this.loadingService.present({
      message: 'Sedang mengunggah foto..',
      duration: 3000
    });
  }

  ambilUlangFoto() {
    this.hapusFotoProduk(this.url);
  }
  
  avatar: any;

  updateFotoProduk(imageData){
    var filename = 'produk/minuman/' + this.id + '/' + this.id_produk + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => {    
      this.loadingUpload(); 
      this.avatar = filename;
    });
  }

  hapusFotoProduk(imageData) {
    var filename = 'produk/minuman/' + this.id + '/' + this.id_produk + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.delete().then(snapshot => {
      this.avatar = null;
    });
  }

  loading:boolean;
  status:boolean;
  simpan()
  {
    this.loadingService.present({
      message: 'Ditunggu ya.',
      duration: 1000
    });
    this.loading=true;
    if(this.avatar != null && this.avatar != undefined) {
      if(this.produk.id == undefined)
      {
        var data={
          nama: this.data.nama,
          status: this.status = true,
          id_warung:this.id,
          kategori: 'minuman',
          ongkir: 'ongkir',
          harga_ongkir: this.data.harga_ongkir,
          harga: this.data.harga,
          stok: this.data.stok,
          url: this.avatar,
          tanggal_tambah: this.timestamp
        };
  
        this.db.collection('produk').doc(this.id_produk).set(data).then(res=>{
          this.loading=false;
          swal({   
            title: "Sukses",   
            text: "Data berhasil ditambahkan.",   
            icon: "success",
            timer: 2000,   
          });
        });
      }
    }
  }

}
