import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahMinumanComponent } from './tambah-minuman.component';

describe('TambahMinumanComponent', () => {
  let component: TambahMinumanComponent;
  let fixture: ComponentFixture<TambahMinumanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahMinumanComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahMinumanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
